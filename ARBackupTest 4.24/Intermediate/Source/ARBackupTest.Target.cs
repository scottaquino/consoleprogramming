using UnrealBuildTool;

public class ARBackupTestTarget : TargetRules
{
	public ARBackupTestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("ARBackupTest");
	}
}
